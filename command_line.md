# Help ISF for Command Line translation

![Command Line](img/command_line.png)

The introduction to the command line, a work to promote Free Software 
around the world. A translation work.

iShareFreedom, better known as isf started the arduous work of 
translating the popular FSF command line introduction book and needs 
your financial support to be able to continue his work.

Make a contribution to his work at the following Bitcoin address: 
bc1q4h3vsl7nf3zk56p8kp3yddnavjc3p25hntqwpn

Get involved with this and other of his free software advocacy work at 
his website: https://libertysoftware.cl/
